# Lupus Decoupled Views

This module adds Custom elements page view display that allows displaying view
with custom elements.

Pagination is also supported out of the box.

Please refer https://lupus-decoupled.org/advanced-topics/views for details.
