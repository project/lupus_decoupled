<?php

declare(strict_types=1);

namespace Drupal\Tests\lupus_decoupled_views\Functional\Views;

use Drupal\Tests\views\Functional\ViewTestBase;

/**
 * Testing custom elements page view.
 */
class CustomElementsPageViewTest extends ViewTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'node',
    'views',
    'file',
    'menu_link_content',
    'custom_elements',
    'lupus_ce_renderer',
    'lupus_decoupled',
    'lupus_decoupled_ce_api',
    'lupus_decoupled_views',
    'lupus_decoupled_views_test',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static array $testViews = ['custom_elements_view_test'];

  /**
   * Nodes to use during this test.
   *
   * @var array
   */
  protected $nodes = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['lupus_decoupled_views_test']): void {
    parent::setUp($import_test_views, $modules);
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);
    $node = [
      'title' => 'Test node 1',
      'status' => TRUE,
      'published' => TRUE,
      'type' => 'page',
    ];
    $this->nodes[] = $this->drupalCreateNode($node);
    $node['title'] = 'Test node 2';
    $this->nodes[] = $this->drupalCreateNode($node);
    $node['title'] = 'Test node 3';
    $this->nodes[] = $this->drupalCreateNode($node);
    $node['title'] = 'Test article 1';
    $node['type'] = 'article';
    $this->nodes[] = $this->drupalCreateNode($node);
  }

  /**
   * Test custom elements page view.
   */
  public function testCustomElementsPageView(): void {
    // Test json response.
    $json_response = json_decode($this->drupalGet('ce-api/view/test-ce', ['query' => ['_content_format' => 'json']]), TRUE);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSame('CE view page', $json_response['title'] ?? []);
    $this->assertSame('drupal-view-custom-elements-view-test', $json_response['content']['element'] ?? []);
    $this->assertCount(4, $json_response['content']['rows'] ?? []);
    $this->assertStringContainsString('Test article', $json_response['content']['rows'][0]['title'] ?? '');
    $this->assertSame('custom_elements_page', $json_response['content']['displayId'] ?? []);
    $this->assertSame('custom_elements_view_test', $json_response['content']['viewId'] ?? []);
    // Test markup response.
    $markup_response = json_decode($this->drupalGet('ce-api/view/test-ce', ['query' => ['_content_format' => 'markup']]), TRUE);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSame('CE view page', $markup_response['title'] ?? '');
    $this->assertStringContainsString('</drupal-view-custom-elements-view-test>', $markup_response['content'] ?? '');
    $this->assertStringContainsString('Test node 3', $markup_response['content'] ?? '');
    $this->assertStringContainsString('Test node 2', $markup_response['content'] ?? '');
    $this->assertStringContainsString('Test node 1', $markup_response['content'] ?? '');
    $this->assertStringContainsString('display-id="custom_elements_page"', $markup_response['content'] ?? '');
    $this->assertStringContainsString('view-id="custom_elements_view_test"', $markup_response['content'] ?? '');
  }

}
