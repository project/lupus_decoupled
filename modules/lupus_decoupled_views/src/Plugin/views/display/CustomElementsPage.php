<?php

namespace Drupal\lupus_decoupled_views\Plugin\views\display;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\custom_elements\CustomElement;
use Drupal\custom_elements\CustomElementGeneratorTrait;
use Drupal\views\Attribute\ViewsDisplay;
use Drupal\views\Plugin\views\display\Page;
use Drupal\views\ViewExecutable;

/**
 * The plugin that handles a full page for a custom elements view.
 *
 * @ingroup views_display_plugins
 */
#[ViewsDisplay(
  id: "custom_elements_page",
  title: new TranslatableMarkup("Custom Elements Page"),
  help: new TranslatableMarkup("Display the view as page rendered with the custom_elements format."),
  uses_menu_links: TRUE,
  uses_route: TRUE,
  contextual_links_locations: ["custom_elements_page"],
  theme: "views_view",
  admin: new TranslatableMarkup("Custom Elements Page"),
)]
class CustomElementsPage extends Page {

  use CustomElementGeneratorTrait;

  /**
   * Pager None class.
   *
   * @var string
   */
  const PAGER_NONE = 'Drupal\views\Plugin\views\pager\None';

  /**
   * Pager Some class.
   *
   * @var string
   */
  const PAGER_SOME = 'Drupal\views\Plugin\views\pager\Some';

  /**
   * {@inheritdoc}
   */
  public function preview() {
    $element = parent::preview();
    $element['#prefix'] = '<div class="preview-section"><pre>';
    $element['#suffix'] = '</pre></div>';
    return $element;
  }

  /**
   * Build custom element from view and rendered display.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param array $element
   *   Rendered display of a view.
   * @param array|null $args
   *   Route parameter arguments.
   *
   * @return \Drupal\custom_elements\CustomElement
   *   Custom element.
   */
  public function buildCustomElement(ViewExecutable $view, array $element, ?array $args = NULL) {
    $custom_element = new CustomElement();
    $custom_element->setTag('drupal-view-' . str_replace('_', '-', $view->id()));
    $custom_element->setAttribute('title', $view->getTitle());
    $custom_element->setAttribute('view_id', $view->id());
    $custom_element->setAttribute('display_id', $view->getDisplay()->getPluginId());
    if (!empty($args)) {
      $custom_element->setAttribute('args', $args);
    }
    $custom_element->setAttribute('pager', empty($view->pager) ? [] : $this->getPagination($view->pager, $element['#rows']['rows'] ?? []));
    $custom_elements = array_filter(array_map(
      fn($row) => $row['#custom_element'] ?? NULL,
      $element['#rows']['rows'] ?? []
    ));
    $custom_element->setSlotFromNestedElements('rows', $custom_elements);
    $custom_element->addCacheableDependency(BubbleableMetadata::createFromRenderArray($element));
    // Allow other modules to change the custom element without replacing the
    // entire method.
    $this->moduleHandler()->alter('lupus_decoupled_views_page_alter', $custom_element);
    return $custom_element;
  }

  /**
   * Gets relevant data from the pager plugin that already 'built' it.
   */
  protected function getPagination($pager, $rows) {
    $pagination = [];
    $class = get_class($pager);
    if ($class === NULL) {
      return NULL;
    }

    if (method_exists($pager, 'getPagerTotal')) {
      $pagination['total_pages'] = $pager->getPagerTotal();
    }
    if (method_exists($pager, 'getCurrentPage')) {
      $pagination['current'] = $pager->getCurrentPage() ?? 0;
    }
    if ($class == static::PAGER_NONE) {
      $pagination['items_per_page'] = $pager->getTotalItems();
    }
    elseif ($class == static::PAGER_SOME) {
      $pagination['total_items'] = count($rows);
    }

    return $pagination;
  }

}
