<?php

namespace Drupal\lupus_decoupled_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\custom_elements\CustomElement;
use Drupal\custom_elements\CustomElementGeneratorTrait;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * The style plugin for custom elements format.
 */
#[ViewsStyle(
  id: "custom_elements",
  title: new TranslatableMarkup("Custom Elements"),
  help: new TranslatableMarkup("Generates the view as JSON."),
  theme: "views_view_custom_elements",
  display_types: ["normal"],
)]
class CustomElements extends StylePluginBase {

  use CustomElementGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // The Serializer parent offers JSON (default) and XML.
    // We only want to make use of JSON so hide the option.
    unset($form['formats']);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $result = ['rows' => []];
    $row_plugin = $this->view->rowPlugin->getPluginId();
    $fields_plugin = $row_plugin === 'fields';

    // If the Data Entity row plugin is used, this will be an array of entities
    // which will pass through Serializer to one of the registered Normalizers,
    // which will transform it to arrays/scalars. If the Data field row plugin
    // is used, $rows will not contain objects and will pass directly to the
    // Encoder.
    foreach ($this->view->result as $row) {
      if ($fields_plugin) {
        $custom_element = new CustomElement();
        foreach ($this->view->field as $name => $value) {
          // @todo Field row plugin is not yet supported and is broken for
          // multi value entity reference fields.
          $custom_element->setAttribute($name, $value->render($row));
        }
      }
      else {
        $custom_element = $this->getCustomElementGenerator()->generate($row->_entity, $this->view->rowPlugin->options['view_mode'] ?? $this->getViewMode($row->_entity->getEntityTypeId(), $row->_entity->bundle()));
      }
      // Explicitly set renderable array containing $custom_element (instead of
      // calling equivalent $custom_element->toRenderArray()), because the
      // "custom_elements_page" display plugin needs $custom_element.
      $result['rows'][] = [
        '#theme' => 'custom_element',
        '#custom_element' => $custom_element,
      ];
    }

    return $result;
  }

  /**
   * Get view mode for Rendered entity view row plugin.
   *
   * @param string $entity_type_id
   *   Entity type id.
   * @param string $bundle
   *   Entity bundle.
   *
   * @return string|null
   *   The view mode must have custom elements display available.
   */
  protected function getViewMode(string $entity_type_id, string $bundle) {
    $options = $this->view->rowPlugin->options;
    if (isset($options['view_modes'])) {
      return $options['view_modes']["entity:$entity_type_id"][$bundle] ?? $options['view_modes']["entity:$entity_type_id"][':default'];
    }
    return NULL;
  }

}
