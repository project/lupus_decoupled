# Lupus Decoupled Block

This module provides support for the blocks layout to Lupus Decoupled Drupal.

It consists of block renderer service and an event subscriber that adds the
blocks of current route into the `blocks` property of response data.

## Usage

Place block(s) to a region at `/admin/structure/block`.

## Further documentation

Please refer to https://lupus-decoupled.org/advanced-topics/block-layout