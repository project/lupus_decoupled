This module
* makes sure that front end pages can be displayed in the responsive-preview
  iframe on the back end;
* makes it so that a correct page is displayed in the pre-existing preview
  toolbar icon showing with individual nodes' layouts;
* adds an extra 'preview link' to the list of devices.
